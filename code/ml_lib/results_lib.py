import numpy as np
import pandas as pd


def open_results(results_file):
    with open(results_file, 'r') as f:
        ls = f.readlines()
        header = [l for l in ls if l.startswith('#')]

        def line_to_rec(l):
            u, i, r = l.split(',')
            return int(u), int(i), float(r)
        recs = [line_to_rec(l) for l in ls if not l.startswith('#')]
        return header, pd.DataFrame(recs, columns=['user', 'item', 'pred'])


def rmse(predicted_ratings):
    """
    Args:
        predicted_ratings: [rating]

    Compute RMSE (Root Mean Squared Error).
    """
    predictions = np.array(predicted_ratings)
    errors = 1 - predictions
    sqr_error = errors ** 2
    mean_sqr_error = np.mean(sqr_error)
    rmse = np.sqrt(mean_sqr_error)

    return rmse
