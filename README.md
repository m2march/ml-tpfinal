#Recomendación de canciones para listas de reproducción
##(Trabajo práctico final de Aprendizaje Automático)

Matías Incem y Martín Miguel

Diciembre de 2015


El objetivo del presente trabajo es investigar la posibilidad de desarrollar un sistema que recomiende canciones
para completar una lista de reproducción. La mayoría de los sistemas comerciales existentes realizan recomendaciones
relacionadas a un usuario en su globalidad. Esto es, en general no tienen en cuenta aspectos locales. Una lista de reproducción
representa un gusto particular por parte del usuario y las canciones se agrupan en la misma por características específicas.
El sistema que buscamos encontraría estas características para poder hacer recomendaciones al criterio particular de la lista.

* [Informe del trabajo](https://bitbucket.org/m2march/ml-tpfinal/raw/master/ml-tpfinal%20Incem%20Miguel.pdf)
