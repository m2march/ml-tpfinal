#!/bin/sh
if [ -d "datasets" ]; then
    echo 'datasets directory already exists. Delete to create again.'
else
    mkdir 'datasets'

    # AOTM 2003
    echo ''
    echo 'Getting aotm2003...'
    mkdir 'datasets/aotm2003'
    wget -P datasets/aotm2003 http://labrosa.ee.columbia.edu/projects/musicsim/aotm_list_ids.txt
    wget -P datasets/aotm2003 http://labrosa.ee.columbia.edu/projects/musicsim/aotm_raw_artists.txt
    wget -P datasets/aotm2003 http://labrosa.ee.columbia.edu/projects/musicsim/aotm_artist_lists.txt
    wget -P datasets/aotm2003 http://labrosa.ee.columbia.edu/projects/musicsim/aotm_aset_lists.txt
    wget -P datasets/aotm2003 http://labrosa.ee.columbia.edu/projects/musicsim/aset400.txt
    wget -P datasets/aotm2003 http://labrosa.ee.columbia.edu/projects/musicsim/aset400-aotmhash.txt

    # AOTM 2011
    echo ''
    echo 'Getting aotm2011...'
    wget http://bmcfee.github.io/data/aotm2011_playlists.json.gz
    gzip -d aotm2011_playlists.json.gz
    mv aotm2011_playlists.json datasets
    rm aotm2011_playlists.json.gz

    # LASTFM
    echo ''
    echo 'Getting lastfm...'
    wget http://mtg.upf.edu/static/datasets/last.fm/lastfm-dataset-1K.tar.gz
    tar -xvzf lastfm-dataset-1K.tar.gz
    mv lastfm-dataset-1K datasets
    rm lastfm-dataset-1K.tar.gz
fi
