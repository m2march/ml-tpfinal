import gflags
import random
import logging
import logging.config
import sys
import re
try:
    import pyspark  # NOQA
except:
    pyspark = None

import ml_lib.experiment_lib as el

from ml_lib import cf, cases

if pyspark is not None:
    from ml_lib import als

logger = logging.getLogger('outside_prediction_experiment')


def output_filename(setup):
    return 'results/o_{recommender}_{sample_size}.out'.format(
        recommender=str(setup.recommender),
        sample_size=setup.sample_size)


def prediction_experiment(recommender, u_i_dict, sample_size, item_set_size,
                          test_percent=0.2):
    logger.info('Experiment for recommender %s', recommender)
    setup = el.ExperimentSetup(recommender, u_i_dict, sample_size, test_percent)
    predict_set = random.sample(setup.total_sample_items(), item_set_size)
    predictions = el.train_and_predict(setup)
    setup.recommender.train(setup.train_dict)

    user_item_ids = [(user_id, item_id)
                     for item_id in predict_set
                     for user_id in setup.total_sample_users()]

    predictions = setup.recommender.predictAll(user_item_ids)
    with open(output_filename(setup), 'w') as outfile:
        outfile.write(str(setup))
        outfile.write('# Item set size: %d\n' % item_set_size)
        outfile.write('# (user_id, item_id, predicted rating)\n')
        for prediction in predictions:
            outfile.write('{}, {}, {}'.format(*prediction) + '\n')


########################

datasets = {
    'aotm2003': cases.get_aotm2003
}

recommenders_keys = ['uxu', 'ixi']
if pyspark:
    recommenders_keys += ['als', 'ials']


# TODO(march): add value constraints
gflags.DEFINE_string('dataset', 'aotm2003', 'dataset name')

gflags.DEFINE_string('sample_size', '5000',
                     'number of users from dataset to use (int|\'all\')',
                     short_name='s')

gflags.DEFINE_list('recommenders', None,
                   ('list of recommenders to use, from list (%s)' %
                    recommenders_keys))

gflags.DEFINE_integer('item_set_size', '1000',
                      'Number of items to predict all users against')

gflags.RegisterValidator('recommenders',
                         lambda rs: all([r in recommenders_keys
                                         for r in rs]),
                         'Not a valid recommender (%s)' % recommenders_keys)

gflags.RegisterValidator('sample_size',
                         lambda s: re.match(r'all|[0-9][0-9]*', s) is not None,
                         'sample_size should be all or a valid integer')

FLAGS = gflags.FLAGS


# TODO(march): make recommenders non-optional
def main(recommender_names, dataset_name, sample_size, item_set_size,
         spark_context):
    als_config = {
        'rank': 30,
        'iterations': 10
    }

    recommenders = {
        'uxu': cf.UserByUserRecommender(),
        'ixi': cf.ItemByItemRecommender(),
    }
    if pyspark is not None:
        recommenders['als'] = als.ALSRecommenderAdapter(
            als_config, spark_context)
        recommenders['ials'] = als.ALSImplicitRecommenderAdapter(
            als_config, spark_context)

    assert set(recommenders_keys) == set(recommenders.keys())

    logger.info(('Performing prediction experiment for recommenders %s, '
                'dataset %s, sample_size %s, item_set_size %d'),
                recommender_names, dataset_name, sample_size, item_set_size)

    u_i_dict = datasets[dataset_name]()
    for rec_name in recommender_names:
        recommender = recommenders[rec_name]
        prediction_experiment(recommender, u_i_dict, sample_size, item_set_size)


if __name__ == '__main__':
    try:
        argv = FLAGS(sys.argv)  # parse flags
    except gflags.FlagsError, e:
        print '%s\\nUsage: %s ARGS\\n%s' % (e, sys.argv[0], FLAGS)
        sys.exit(1)

    logging.config.dictConfig({
        'version': 1,
        'formatters': {
            'basic': {
                'format': '%(asctime)s %(levelname)s %(name)s :: %(message)s'
            }
        },
        'handlers': {
            'console': {
                'class': 'logging.StreamHandler',
                'stream': 'ext://sys.stdout',
                'formatter': 'basic',
                'level': 'INFO'
            },
            'log_file': {
                'class': 'logging.FileHandler',
                'filename': 'outside_prediction_experiment.log',
                'formatter': 'basic',
                'level': 'DEBUG'
            }
        },
        'loggers': {
            'ALSRecommenderAdapter': {
                'level': 'DEBUG',
                'handlers': ['console', 'log_file'],
                'formatter': 'basic'
            },
            'ItemByItemRecommender': {
                'level': 'DEBUG',
                'handlers': ['console', 'log_file'],
                'formatter': 'basic'
            },
            'UserByUserRecommender': {
                'level': 'DEBUG',
                'handlers': ['console', 'log_file'],
                'formatter': 'basic'
            },
            'outside_prediction_experiment': {
                'level': 'DEBUG',
                'handlers': ['console', 'log_file'],
                'formatter': 'basic'
            }
        },
        'root': {
            'level': logging.DEBUG
        }
    })

    if pyspark:
        spark_conf = (pyspark.SparkConf()
                      .setAppName('Prediction Experiment')
                      )
        spark_context = pyspark.SparkContext(conf=spark_conf)
    else:
        spark_context = None
    main(FLAGS.recommenders, FLAGS.dataset,
         FLAGS.sample_size, FLAGS.item_set_size,
         spark_context)
