"""This module contains all our recommenders. Each recommender is contained
in a class that initializes with the constructor and has a predict function."""

import numpy as np
import logging

from pympler import summary, muppy, asizeof


def users_dict_to_matrix(users_dict):
    songs = sorted(set([s for u in users_dict.values() for s in u]))
    songs_mapping = dict([(id, idx) for idx, id in enumerate(songs)])
    users = sorted(users_dict.keys())
    users_mapping = dict([(id, idx) for idx, id in enumerate(users)])
    r = np.zeros((len(users_dict), len(songs)), dtype='bool')
    for user_id, playlist in users_dict.items():
        for song_id in playlist:
            r[users_mapping[user_id]][songs_mapping[song_id]] = 1

    # R Matrix, songs_to_int, int_to_songs, users_to_int, int_to_users)
    return (r, songs, songs_mapping, users, users_mapping)


class Recommender():

    def __init__(self):
        """
        Intializes a newly configured recomender model.
        """
        pass

    def _items_dict(self, users_dict):
        'Returns a item_id -> [user_id] dictionary'
        items_dict = {}
        for user_id, items in users_dict.items():
            for item_id in items:
                if item_id not in items_dict:
                    items_dict[item_id] = []
                items_dict[item_id].append(user_id)
        return items_dict

    def _pre_train(self, users_dict):
        """
        Trains the model on the given dataset.

        Params:
            users_dict: diccionario usuario -> [item_id]
        """
        self.r_dict = users_dict
        self.users_count = len(users_dict)
        self.items_dict = self._items_dict(users_dict)

    def _print_sizes(self, *self_attributes):
        """
        Args:
            self_attributes: [(var_name, var)]
        """
        s = 'after training sizes: %s'
        sizes_strs = [('%s: (%d elems, %d bytes)' %
                      (var_name, len(var), asizeof.asizeof(var)))
                      for var_name, var in self_attributes]
        self.logger.debug(s, ' '.join(sizes_strs))

    def predict(self, user_id, item_id):
        """
        Predict the rating given the training data.

        Returns:
            None if system has no prediction over the item,
            otherwise prediction between 0 and 1
        """
        raise NotImplementedError()

    def predictAll(self, user_item_ids):
        """
        Args:
            user_item_ids : [(user_id, item_id)]

        Returns prediction for each tuple :: [(user_id, item_id, rating)]
        """
        ret = []
        self.logger.debug('predicting for all tuples (%d): %s',
                          len(user_item_ids), user_item_ids)
        for idx, (user_id, item_id) in enumerate(user_item_ids):
            ret.append((user_id, item_id, self.predict(user_id, item_id)))
        ret = [p for p in ret if p[2] is not None]
        self.logger.debug('predictions for all tuples (%d): %s',
                          len(ret), ret)
        return ret

    def __str__(self):
        "Returns an string identifing the recommender instance"
        raise NotImplementedError()

    def desc(self):
        return {'name': str(self)}


class UserByUserRecommender(Recommender):

    logger = logging.getLogger('UserByUserRecommender')

    def train(self, users_dict):
        self._pre_train(users_dict)
        self._calculate_all_similitudes()
        self._user_total_sim_cache = {}
        self._print_sizes(('r_dict', self.r_dict),
                          ('_items_dict', self.items_dict),
                          ('_sim_matrix', self._sim_matrix))

    def _jaccard_sim(self, active_user, user):
        '''http://en.wikipedia.org/wiki/Jaccard_index
        Params:
            active_user: item_ids of active user
            user: item_ids of other user

        Returns:
            jaccard distance between users
        '''
        active_user_set = set(active_user)
        inter = len(active_user_set.intersection(user))
        union = len(active_user_set.union(user))
        return float(inter) / union

    def _calculate_all_similitudes(self):
        self._sim_matrix = dict()
        users_ids = sorted(self.r_dict.keys())
        self.logger.info('Calculating similarities for %d users',
                         len(users_ids))
        seen_items = 0
        seen_users = 0
        stored_sims = 0
        for i in xrange(self.users_count):
            if i % 500 == 0:
                self.logger.info('Similarities for user number %d', i)
                self.logger.debug('Seen items %d, seen users %d, stored sims %d'
                                  'dict size: (%d, %d)',
                                  seen_items, seen_users, stored_sims,
                                  len(self._sim_matrix),
                                  asizeof.asizeof(self._sim_matrix))
                all_objects = muppy.get_objects()
                summ = sorted(summary.summarize(all_objects),
                              key=lambda x: x[1], reverse=True)
                str_summary = ['\t' + str(x)
                               for x in summ][:30]
                self.logger.debug('Object summary: \n' +
                                  '\n'.join(str_summary))
            a_id = users_ids[i]
            self._sim_matrix[a_id] = dict()
            for j in xrange(i+1, self.users_count):
                b_id = users_ids[j]
                sim = self._jaccard_sim(
                    self.r_dict[a_id],
                    self.r_dict[b_id]
                )
                if sim != 0:
                    self._sim_matrix[a_id][b_id] = sim
                    stored_sims += 1
                seen_users += 1
                seen_items += len(self.r_dict[b_id])

    def sim(self, user_a_id, user_b_id):
        min_user = min(user_a_id, user_b_id)
        max_user = max(user_a_id, user_b_id)
        if max_user in self._sim_matrix[min_user]:
            return self._sim_matrix[min_user][max_user]
        else:
            return 0.0

    def total_sim_per_user(self, user_id):
        if user_id in self._user_total_sim_cache:
            return self._user_total_sim_cache[user_id]
        else:
            _total_sim = sum([self.sim(user_id, o_user_id)
                              for o_user_id in self.r_dict.keys()
                              if o_user_id != user_id])
            self._user_total_sim_cache[user_id] = _total_sim
            return _total_sim

    def predict(self, user_id, item_id):
        """
        see: recommender-systems-eml2010.pdf
        """
        #  The system can't say anything
        if (item_id not in self.items_dict
           or self.items_dict[item_id] == [user_id]):
            return None

        total_weights = self.total_sim_per_user(user_id)
        if not total_weights:
            return None

        predict_weights = 0.
        for other_user_id in self.items_dict[item_id]:
            if other_user_id != user_id:
                sim = self.sim(user_id, other_user_id)
                predict_weights += sim
        return predict_weights / total_weights

    def __str__(self):
        return 'uxu'


class ItemByItemRecommender(Recommender):

    logger = logging.getLogger('ItemByItemRecommender')

    def train(self, users_dict):
        self._pre_train(users_dict)
        self._sim_cache = dict()
        self._print_sizes(('r_dict', self.r_dict),
                          ('_items_dict', self.items_dict))

    def _item_sim(self, item_i, item_j):
        item_i_set = set(item_i)
        inter = len(item_i_set.intersection(item_j))
        union = len(item_i_set.union(item_j))
        return float(inter) / union

    def sim(self, item_a_id, item_b_id):
        min_item = min(item_a_id, item_b_id)
        max_item = max(item_a_id, item_b_id)
        if (min_item in self._sim_cache
           and max_item in self._sim_cache[min_item]):
                return self._sim_cache[min_item][max_item]
        else:
            if (item_a_id not in self.items_dict or
               item_b_id not in self.items_dict):
                _sim = None
            else:
                _sim = self._item_sim(self.items_dict[item_a_id],
                                      self.items_dict[item_b_id])
            if min_item not in self._sim_cache:
                self._sim_cache[min_item] = dict()
            self._sim_cache[min_item][max_item] = _sim
            return _sim

    def predict(self, user_id, item_id):
        """
        see: recommender-systems-eml2010.pdf
        """
        items = self.r_dict[user_id]
        similitudes = [self.sim(item_id, o_item_id)
                       for o_item_id in items]
        f_similitudes = [s for s in similitudes
                         if s is not None]
        if len(f_similitudes) == 0:
            return None
        predict_weights = sum(f_similitudes)
        return predict_weights / len(items)

    def __str__(self):
        return 'ixi'
