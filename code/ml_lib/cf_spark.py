"""This module contains all our recommenders. Each recommender is contained
in a class that initializes with the constructor and has a predict function."""

import numpy as np
import sys


def users_dict_to_matrix(users_dict):
    songs = sorted(set([s for u in users_dict.values() for s in u]))
    songs_mapping = dict([(id, idx) for idx, id in enumerate(songs)])
    users = sorted(users_dict.keys())
    users_mapping = dict([(id, idx) for idx, id in enumerate(users)])
    r = np.zeros((len(users_dict), len(songs)), dtype='bool')
    for user_id, playlist in users_dict.items():
        for song_id in playlist:
            r[users_mapping[user_id]][songs_mapping[song_id]] = 1

    # R Matrix, songs_to_int, int_to_songs, users_to_int, int_to_users)
    return (r, songs, songs_mapping, users, users_mapping)


class Recommender():

    def __init__(self):
        """
        Intializes a newly configured recomender model.
        """
        pass

    def _items_dict(self, users_dict):
        'Returns a item_id -> [user_id] dictionary'
        items_dict = {}
        for user_id, items in users_dict.items():
            for item_id in items:
                if item_id not in items_dict:
                    items_dict[item_id] = []
                items_dict[item_id].append(user_id)
        return items_dict

    def pre_train(self, users_dict):
        """
        Trains the model on the given dataset.

        Params:
            users_dict: diccionario usuario -> [item_id]
        """
        self.r_dict = users_dict
        self.users_count = len(users_dict)

    def predict(self, user_id, item_id):
        """
        Predict the rating given the training data.
        """
        raise NotImplementedError()

    def predictAll(self, user_item_ids):
        """
        Args:
            user_item_ids : [(user_id, item_id)]

        Returns prediction for each tuple
        """
        total_predictions = len(user_item_ids)
        for idx, (user_id, item_id) in enumerate(user_item_ids):
            sys.stderr.write('\r%f%%' % (float(idx) / total_predictions * 100))
            yield self.predict(user_id, item_id)


def _parallel_calc_similitudes(users_count, r_dict,
                               _jaccard_sim, spark_context):
    def sim_i_j(bc_r_dict, bc_users_ids, i, j):
        a_id = bc_users_ids.value[i]
        b_id = bc_users_ids.value[j]
        return (a_id, b_id, _jaccard_sim(
            bc_r_dict.value[a_id],
            bc_r_dict.value[b_id]
        ))

    users_ids = sorted(r_dict.keys())

    bc_users_ids = spark_context.broadcast(users_ids)
    bc_r_dict = spark_context.broadcast(r_dict)

    users_pairs = [(i, j)
                   for i in xrange(users_count)
                   for j in xrange(i+1, users_count)]

    users_pairs_rdd = spark_context.parallelize(users_pairs, 4 * 3)
    similarity_rdd = users_pairs_rdd.map(
        lambda (i, j): sim_i_j(bc_r_dict, bc_users_ids, i, j))

    return similarity_rdd.collect()


def _jaccard_sim(active_user, user):
    '''http://en.wikipedia.org/wiki/Jaccard_index
    Params:
        active_user: item_ids of active user
        user: item_ids of other user

    Returns:
        jaccard distance between users
    '''
    active_user_set = set(active_user)
    inter = len(active_user_set.intersection(user))
    union = len(active_user_set.union(user))
    return float(inter) / union


def _sim(bc_sim_matrix, user_a_id, user_b_id):
    min_user = min(user_a_id, user_b_id)
    max_user = max(user_a_id, user_b_id)
    return bc_sim_matrix.value[min_user][max_user]


class UserByUserRecommender(Recommender):

    def __init__(self, spark_context):
        self.spark_context = spark_context

    def train(self, users_dict):
        self.pre_train(users_dict)
        self._calculate_all_similitudes()
        self.items_dict = self._items_dict(users_dict)
        self._calculate_total_sim_per_user()

    def _calculate_all_similitudes(self):
        similarities = _parallel_calc_similitudes(
            self.users_count, self.r_dict,
            _jaccard_sim, self.spark_context
        )

        self._sim_matrix = dict()

        for a_id, b_id, sim in similarities:
            min_id = min(a_id, b_id)
            max_id = max(a_id, b_id)
            if min_id not in self._sim_matrix:
                self._sim_matrix[min_id] = dict()
            self._sim_matrix[min_id][max_id] = sim

    def _calculate_total_sim_per_user(self):
        users_rdd = self.spark_context.parallelize(self.r_dict.keys(), 4 * 3)
        bb_users_ids = self.spark_context.broadcast(self.r_dict.keys())
        bb_sim_matrix = self.spark_context.broadcast(self._sim_matrix)
        total_sim_per_user_pairs = users_rdd.map(
            lambda u: (u, sum((_sim(bb_sim_matrix, u, o_user_id)
                               for o_user_id in bb_users_ids.value
                               if o_user_id != u)) - 1
                       )).collect()

        self._user_total_sim_cache = dict(total_sim_per_user_pairs)

    def sim(self, user_a_id, user_b_id):
        min_user = min(user_a_id, user_b_id)
        max_user = max(user_a_id, user_b_id)
        return self._sim_matrix[min_user][max_user]

    def total_sim_per_user(self, user_id):
        if user_id in self._user_total_sim_cache:
            return self._user_total_sim_cache[user_id]
        else:
            raise KeyError(
                'User by user_id=%d has no cached total similarity' % user_id)

    def predict(self, user_id, item_id):
        total_weights = self.total_sim_per_user(user_id)
        predict_weights = 0.
        if item_id in self.items_dict:
            for other_user_id in self.items_dict[item_id]:
                if other_user_id != user_id:
                    sim = self.sim(user_id, other_user_id)
                    predict_weights += sim
        return predict_weights / total_weights if total_weights else 0


class ItemByItemRecommender(Recommender):

    def train(self, users_dict):
        self.pre_train(users_dict)
        self.items_dict = self._items_dict(users_dict)
        self._sim_cache = dict()

    def _item_sim(self, item_i, item_j):
        item_i_set = set(item_i)
        inter = len(item_i_set.intersection(item_j))
        union = len(item_i_set.union(item_j))
        return float(inter) / union

    def sim(self, item_a_id, item_b_id):
        min_item = min(item_a_id, item_b_id)
        max_item = max(item_a_id, item_b_id)
        if (min_item in self._sim_cache
            and max_item in self._sim_cache[min_item]):
                return self._sim_cache[min_item][max_item]
        else:
            if (item_a_id not in self.items_dict or
                item_b_id not in self.items_dict):
                _sim = 0
            else:
                _sim = self._item_sim(self.items_dict[item_a_id],
                                      self.items_dict[item_b_id])
            if min_item not in self._sim_cache:
                self._sim_cache[min_item] = dict()
            self._sim_cache[min_item][max_item] = _sim
            return _sim

    def predict(self, user_id, item_id):
        items = self.r_dict[user_id]
        predict_weights = sum((self.sim(item_id, o_item_id)
                               for o_item_id in items))
        return predict_weights / len(items)
