'''This script trains recommenders and stores the trained model.'''

import gflags
import logging
import logging.config
import sys
import cPickle

import ml_lib.experiment_lib as el

try:
    import pyspark  # NOQA
except:
    pyspark = None

from ml_lib import cf
if pyspark is not None:
    from ml_lib import als

logger = logging.getLogger('train_recommender')

##################

recommenders_keys = ['uxu', 'ixi']
if pyspark:
    recommenders_keys += ['als', 'ials']

gflags.DEFINE_string('setup', None,
                     'Experiment setup pickle filename')

gflags.DEFINE_list('recommenders', None,
                   ('list of recommenders to use, from list (%s)' %
                    recommenders_keys))

gflags.MarkFlagAsRequired('setup')

gflags.RegisterValidator('recommenders',
                         lambda rs: all([r in recommenders_keys
                                         for r in rs]),
                         'Not a valid recommender (%s)' % recommenders_keys)

FLAGS = gflags.FLAGS


def main(recommender_names, setup_file, spark_context):
    als_config = {
        'rank': 30,
        'iterations': 10
    }

    recommenders = {
        'uxu': lambda: cf.UserByUserRecommender(),
        'ixi': lambda: cf.ItemByItemRecommender(),
    }
    if pyspark is not None:
        recommenders['als'] = lambda: als.ALSRecommenderAdapter(
            als_config, spark_context)
        recommenders['ials'] = lambda: als.ALSImplicitRecommenderAdapter(
            als_config, spark_context)

    with open(setup_file) as f:
        setup = cPickle.load(f)

        logger.info(('Training recommenders %s with setup %s'),
                    recommender_names, setup_file)

        for rec_name in recommender_names:
            logger.info('Training {} recommender'.format(rec_name))
            trained_rec = recommenders[rec_name]()
            trained_rec.train(setup.train_dict)
            trained_rec.setup_id = setup.id
            outfile_basename = 'trained_recs/{}_s{}.trained'.format(
                str(trained_rec), setup.sample_size)
            outfile_name = el.save_recommender(trained_rec, outfile_basename)
            logger.info('Trained {} recommender stored at {}'.format(
                rec_name, outfile_name))


if __name__ == '__main__':
    try:
        argv = FLAGS(sys.argv)  # parse flags
    except gflags.FlagsError, e:
        print '%s\\nUsage: %s ARGS\\n%s' % (e, sys.argv[0], FLAGS)
        sys.exit(1)

    logging.config.dictConfig({
        'version': 1,
        'formatters': {
            'basic': {
                'format': '%(asctime)s %(levelname)s %(name)s :: %(message)s'
            }
        },
        'handlers': {
            'console': {
                'class': 'logging.StreamHandler',
                'stream': 'ext://sys.stdout',
                'formatter': 'basic',
                'level': 'INFO'
            },
            'log_file': {
                'class': 'logging.FileHandler',
                'filename': 'test_prediction_experiment.log',
                'formatter': 'basic',
                'level': 'DEBUG'
            }
        },
        'loggers': {
            'ALSRecommenderAdapter': {
                'level': 'DEBUG',
                'handlers': ['console', 'log_file'],
                'formatter': 'basic'
            },
            'ItemByItemRecommender': {
                'level': 'DEBUG',
                'handlers': ['console', 'log_file'],
                'formatter': 'basic'
            },
            'UserByUserRecommender': {
                'level': 'DEBUG',
                'handlers': ['console', 'log_file'],
                'formatter': 'basic'
            },
            'train_recommender': {
                'level': 'INFO',
                'handlers': ['console', 'log_file'],
                'formatter': 'basic'
            },
        },
        'root': {
            'level': logging.DEBUG
        }
    })

    if pyspark:
        spark_conf = (pyspark.SparkConf()
                      .setAppName('Prediction Experiment')
                      )
        spark_context = pyspark.SparkContext(conf=spark_conf)
    else:
        spark_context = None
    main(FLAGS.recommenders, FLAGS.setup, spark_context)
