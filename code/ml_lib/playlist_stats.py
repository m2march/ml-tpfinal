import random
import collections
import sys

import numpy as np

from ascii_graph import Pyasciigraph


def get_user_by_user_interaction(playlist_data, sample_size):
    users = random.sample(playlist_data.values(),
                          min(sample_size, len(playlist_data)))
    user_by_user_intersection = np.zeros((len(users), len(users)), dtype=np.int)
    print 'matrix size: %f MB | %d numbers' % \
        (user_by_user_intersection.nbytes / (1024 ** 2),
         user_by_user_intersection.size / 2)

    for i in xrange(len(users)):
        i_set = set(users[i])
        user_by_user_intersection[i, i] = -1
        sys.stderr.write(('Calculating user by user interaction... %d/%d    \r'
                          % (i, len(users))))
        for j in xrange(i + 1, len(users)):
            inter = len(i_set.intersection(users[j]))
            user_by_user_intersection[i][j] = -1
            user_by_user_intersection[j][i] = inter
    return user_by_user_intersection


def get_user_by_user_histogram(playlist_data, sample_size):
    users = random.sample(playlist_data.values(),
                          min(sample_size, len(playlist_data)))
    histo = dict()
    for u in users:
        pass


def print_playlist_generic_stats(playlist_data, title):
    title = '# %s #' % title
    print '#' * len(title)
    print title
    print '#' * len(title)
    print ''
    print '#Playlists:', len(playlist_data)
    print '#Songs:', len(set([s for pl in playlist_data.values()
                              for s in pl]))
    print ''
    print 'Songs per playlist:'

    songs_histo = sorted(collections.Counter(
        [len(pl) for pl in playlist_data.values()]).items(),
        key=lambda x: x[0], reverse=True)
    for line in Pyasciigraph(graph_char='x').graph('tuvieja', songs_histo)[2:]:
        print line
