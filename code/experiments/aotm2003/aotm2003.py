# -*- coding: utf-8 -*-
import gflags
import random
import sys
import cPickle
import os

import pandas as pd
import numpy as np
import matplotlib
import matplotlib.pyplot as plt

from ml_lib import cases


gflags.DEFINE_integer('sample_size', 5000,
                      'Number of playlist to use')

FLAGS = gflags.FLAGS

try:
    sys.argv = FLAGS(sys.argv)  # parse flags
except gflags.FlagsError, e:
    print '%s\\nUsage: %s ARGS\\n%s' % (e, sys.argv[0], FLAGS)
    sys.exit(1)

matplotlib.style.use('ggplot')

aotm = cases.get_aotm2003()

# Generic statistics
with open('results/aotm2003.txt', 'w') as of:
    of.write('#Playlists: %d' % len(aotm))
    of.write('#Songs: %d' % len(
        set([s for pl in aotm.values() for s in pl])
    ))

playlist_lengths = pd.Series([len(pl) for pl in aotm.values()])
plt.figure()
playlist_lengths.plot(kind='hist', bins=25, color='green')
plt.xlabel(u'Distribución de largo de listas')
plt.ylabel(u'Canciones por lista')
plt.savefig('results/aotm2003_length_hist.pdf')


def inter(a, b):
    return len(set(a).intersection(b))

# UxU interaction
if not os.path.exists('results/uxu_inter.pkl'):
    playlist_sample = random.sample(aotm.values(), FLAGS.sample_size)
    uxu_interaction = {}

    for idx, a in enumerate(playlist_sample):
        if idx % 100 == 0:
            print idx
        for b in playlist_sample[idx+1:]:
            i = inter(a, b)
            if i not in uxu_interaction:
                uxu_interaction[i] = 0
            uxu_interaction[i] = uxu_interaction[i] + 1

    with open('results/uxu_inter.pkl', 'w') as of:
        cPickle.dump(uxu_interaction, of)
else:
    with open('results/uxu_inter.pkl', 'r') as of:
        uxu_interaction = cPickle.load(of)

hist = np.array(uxu_interaction.values())
left = np.array(uxu_interaction.keys())
hist = hist / float(hist.sum())
plt.figure()
plt.bar(height=hist, left=left, width=1, color='orange', log=True)
plt.xlabel(u'Canciones en común')
plt.ylabel(ur'Porcentaje de pares de usuarios con $n$ canciones en común')
plt.savefig('results/uxu_inter.pdf')
