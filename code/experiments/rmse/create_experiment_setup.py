'''Script for creating the experimental setup for recommender system training.

The setup will start from a dataset and create a train set and test set.'''

import gflags
import random
import cPickle
import sys
import ml_lib.experiment_lib as el


gflags.DEFINE_string('dataset', 'aotm2003',
                     'Base dataset to use')

gflags.DEFINE_string(
    'sample_size', '10000',
    'Size of the sample taken from dataset (in amount of users)',
    short_name='s')

gflags.DEFINE_float('test_percent', 0.2,
                    'Percentage of items per user to use in the test set')

gflags.DEFINE_integer('explore_set_size', 10000,
                      'Number of items to use in outside exploration part',
                      short_name='e')

gflags.DEFINE_string('outfile', None,
                     'Name of the file where to record the setup')

FLAGS = gflags.FLAGS


def produce_outfile_name(dataset, sample_size):
    return 'setups/{}_s{}.setup.pkl'.format(dataset, sample_size)


def main(dataset, sample_size, test_percent=0.2, explore_set_size=10000,
         outfile=None):
    setup = el.ExperimentSetup(dataset, sample_size, test_percent,
                            explore_set_size)
    if outfile is None:
        outfile = produce_outfile_name(dataset, sample_size)

    print 'Creating setup file %s' % outfile
    with open(outfile, 'w') as f:
        cPickle.dump(setup, f)


if __name__ == '__main__':
    try:
        argv = FLAGS(sys.argv)  # parse flags
    except gflags.FlagsError, e:
        print '%s\\nUsage: %s ARGS\\n%s' % (e, sys.argv[0], FLAGS)
        sys.exit(1)

    main(FLAGS.dataset, FLAGS.sample_size, FLAGS.test_percent,
         FLAGS.explore_set_size, FLAGS.outfile)
