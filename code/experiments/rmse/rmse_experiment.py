"""Recommender evaluation methods"""

import als
import cases
import cf
import cf_spark
import math
import matplotlib.pyplot as plt
import numpy as np
import pyspark  # Injected by spark
import random
import logging
import logging.config
import itertools


logger = logging.getLogger('RMSE Experiment')


def calculate_prediction_ratings(recommender, test_dict):
    """
    Args:
        recommender: instance of a cf.Recommender
        test_dict: user_id (playlist) -> [item_id] (songs) dictionary

    Returns user_id -> [rating] (predicted_ratings)
    """
    logger.debug('calculate_prediction_ratings: predictingAll (%d, %d) %s',
                 len(test_dict),
                 sum([len(items) for items in test_dict.values()]),
                 str([len(items) for items in test_dict.values()]))

    predictions = recommender.predictAll([
        (user_id, item_id)
        for user_id, items_ids in test_dict.items()
        for item_id in items_ids
    ])

    logger.debug('calculate_prediction_ratings: predictions (%d)',
                 len(predictions))

    sorted(predictions, key=lambda x: x[0])
    logger.debug('calculate_prediction_ratings: prediction smaller than 0: %d, '
                 'prediction grater than 1: %d',
                 len([1 for _, item in predictions if item < 0]),
                 len([1 for _, item in predictions if item > 1]))

    ret = dict([(user_id, [x[1] for x in ps])
                for user_id, ps in itertools.groupby(
                    predictions, key=lambda x: x[0])
                ])
    logger.debug('calculate_prediction_ratings: grouped_predictions (%d, %d) %s',
                 len(ret),
                 sum([len(items) for items in ret.values()]),
                 str([len(items) for items in ret.values()]))
    return ret


def calculate_rmse(predicted_ratings):
    """
    Args:
        predicted_ratings: [rating]

    Compute RMSE (Root Mean Squared Error).
    """
    predictions = np.array(predicted_ratings)
    errors = 1 - predictions
    sqr_error = errors ** 2
    mean_sqr_error = np.mean(sqr_error)
    rmse = np.sqrt(mean_sqr_error)

    return rmse


def split_dataset(u_i_dict, test_percent=0.2):
    """
    Splits the dataset into train and test dictionaries.
    To split the dataset, de user-item dictionary is split into two dictionaries
    with the same keys as the original but with the values split in the given
    reason.

    Args:
        u_i_dict: user_id (playlist) -> [item_id] (songs)
        test_percent: ideal percentage of train items split as test

    Returns:
        (train_dict, test_dict, actual percentage of total items in test)
    """
    test_dict = {}
    train_dict = {}
    for user_id, items_ids in u_i_dict.items():
        random.shuffle(items_ids)
        slice_idx = int(math.floor(len(items_ids) * test_percent))
        test_dict[user_id] = items_ids[:slice_idx]
        train_dict[user_id] = items_ids[slice_idx:]

    total_items = sum([len(v) for v in u_i_dict.values()])
    test_items = sum([len(v) for v in test_dict.values()])
    test_percent = float(test_items) / total_items
    return (train_dict, test_dict, test_percent)


def normed_hist(values, bins):
    hist = np.histogram(values, bins=bins)[0]
    logger.debug('normed_hist :: #: %s h: %s s: %s b: %s, v: %s',
                 len(values), str(hist), hist.sum(),
                 bins, values)
    return hist / float(hist.sum())


def perform_experiments_on_rec(recommender, train_dict, test_dict):
    """
    Performs experiments on the given recommender with the train and test sets.

    Args:
        recommender :: cf.Recommender
        train_dict :: user_id (playlist) -> [item_id] (songs), training data
        test_dict :: user_id (playlist) -> [item_id], test data. songs in this
            dictionary are known to belong to the user, but are not known in the
            training data.

    Returns:

    """
    logger.debug('train_dict values lengths (%d): %s', len(train_dict),
                 str([len(items) for items in train_dict.values()]))
    logger.debug('test_dict values lengths (%d): %s', len(test_dict),
                 str([len(items) for items in test_dict.values()]))
    logger.debug('users with no test items (%d)',
                 len([1 for items in test_dict.values() if len(items) == 0]))
    # Training
    recommender.train(train_dict)

    # Predictions
    predictions = calculate_prediction_ratings(recommender, test_dict)
    logger.debug('predictions values lengths (%d): %s', len(predictions),
                 str([len(items) for items in predictions.values()]))

    # Averaged per user historgram
    per_user_hist_bins = np.linspace(0, 1, num=6)
    per_user_hist = dict([(user_id,
                           normed_hist(ratings, bins=per_user_hist_bins))
                          for user_id, ratings in predictions.items()])
    logger.debug('per_user_hist: %s', str(per_user_hist))
    logger.debug('summed_per_user_hist: %s', str(sum(per_user_hist)))
    summed_user_hist = sum(per_user_hist.values()) / float(len(per_user_hist))

    per_user_rmse = [calculate_rmse(ratings)
                     for _, ratings in predictions.items()]
    logger.debug('per_user_rmse: %s', str(per_user_rmse))
    user_rmse_bins = np.linspace(0, 1, num=11)
    user_rmse_hist = normed_hist(per_user_rmse, bins=user_rmse_bins)

    all_ratings = [rating
                   for ratings in predictions.values()
                   for rating in ratings]
    logger.debug('all_ratings: %s', all_ratings)
    global_rmse = calculate_rmse(all_ratings)

    return {
        'global_rmse': global_rmse,
        'summed_user_hist': (summed_user_hist, per_user_hist_bins),
        'user_rmse_hist': (user_rmse_hist, user_rmse_bins),
    }


def experiment(recomenders,
               sample_size=5000,
               repeats=4,
               test_percent=0.2,
               data=cases.get_aotm2003()):
    """
    Args:
        data :: user_id -> [item_id]

    Returns:
        recommender_name -> [experiment_results]
        with len(ret[_]) == repeats
    """

    results = dict([(name, []) for name, _ in recomenders])
    for repeat in xrange(repeats):
        sample = dict(random.sample(data.items(), sample_size))
        train_dict, test_dict, split_percent = split_dataset(sample,
                                                             test_percent)
        for name, rec in recomenders:
            rmse = perform_experiments_on_rec(rec, train_dict, test_dict)
            results[name].append(rmse)

    return results


def plot_hist(heights, bins):
    total_width = bins[1]
    bar_width = total_width * 0.9
    left = bins + (total_width - bar_width) * 0.5
    plt.bar(left=left[:-1], height=heights, width=bar_width)
    plt.xticks(bins)


def present_results(experiment_results):
    """
    Outputs results information.

    Args:
        experiment_results :: rec_name -> [ :: experiment_result ]
        experiment_result is the result of perform_experiments_on_rec
    """

    plt.style.use('ggplot')
    with open('results/rmse.txt', 'w') as rmse_file:
        rmse_file.write('# Recommender, rmse\n')
        for rec_name, repeats in results.items():
            result = repeats[0]
            rmse_file.write('%s, %.5f\n' % (rec_name, result['global_rmse']))

            # User rmse hist
            plt.figure()
            plot_hist(*result['user_rmse_hist'])
            plt.title('User RMSE histogram (%s)' % rec_name)
            plt.xlabel('users with rmse')
            plt.ylabel('percentage')
            plt.savefig('results/user_rmse_hist-%s.pdf' % rec_name)

            # User rmse hist
            plt.figure()
            plot_hist(*result['summed_user_hist'])
            plt.title('User averaged rating histogram (%s)' % rec_name)
            plt.xlabel('average songs in users with rmse')
            plt.ylabel('percentage')
            plt.savefig('results/summed_user_hist-%s.pdf' % rec_name)


def print_results(results):
    for rec_name, repeats in results.items():
        for repeat in repeats:
            print rec_name
            print repeat


if __name__ == '__main__':
    logging.config.dictConfig({
        'version': 1,
        'handlers': {
            'console': {
                'class': 'logging.StreamHandler',
                'stream': 'ext://sys.stderr',
            }
        },
        'formatters': {
            'basic': {
                'format': '%(levelname)s %(name)s :: %(message)s'
            }
        },
        'loggers': {
            'ALSRecommenderAdapter': {
                'level': 'DEBUG',
                'handlers': ['console'],
                'formatter': 'basic'
            },
            'UserByUserRecommender': {
                'level': 'DEBUG',
                'handlers': ['console'],
                'formatter': 'basic'
            },
            'ItemByItemRecommender': {
                'level': 'DEBUG',
                'handlers': ['console'],
                'formatter': 'basic'
            },
            'RMSE Experiment': {
                'level': 'DEBUG',
                'handlers': ['console'],
                'formatter': 'basic'
            }
        },
        'root': {
            'level': logging.DEBUG
        }
    })
    spark_conf = (pyspark.SparkConf()
                  .setAppName('RMSE Experiment')
                  )
    spark_context = pyspark.SparkContext(conf=spark_conf)

    als_config = {
        'rank': 30,
        'iterations': 10
    }

    one_of_each_recs = [
        ('User by User', cf.UserByUserRecommender()),
        ('Item by Item', cf.ItemByItemRecommender()),
        ('ALS', als.ALSRecommenderAdapter(als_config, spark_context)),
        ('Implicit ALS', als.ALSImplicitRecommenderAdapter(
            als_config, spark_context)),
    ]

    als_rank_explore = [
        ('ALS r(%d) i(%d)' % (r, 30), als.ALSRecommenderAdapter(
            als.als_train_f(**{'rank': r, 'iterations': 5}), spark_context))
        for r in [200, 100, 50, 30, 15]
    ]

    als_iter_explore = [
        ('ALS r(%d) i(%d)' % (30, iter), als.ALSRecommenderAdapter(
            als.als_train_f(**{'rank': 30, 'iterations': iter}), spark_context))
        for iter in [5, 10, 15, 30]
    ]

    test_recs = [
        # ('Spark-User by User', cf_spark.UserByUserRecommender(spark_context)),
        # ('User by User', cf.UserByUserRecommender()),
        # ('Item by Item', cf.ItemByItemRecommender()),
        ('ALS', als.ALSRecommenderAdapter(
            als.als_train_f(**als_config), spark_context)),
        # ('Implicit ALS', als.ALSRecommenderAdapter(
        #     als.als_implicit_train_f(**als_config), spark_context)),
    ]

    recs = test_recs
    sample_size = 20000
    repeats = 1
    logger.info('Starting experiment with sample_size: %d, repeats: %d, '
                'recommenders: %s', sample_size, repeats,
                str([x[0] for x in recs]))
    results = experiment(recs, sample_size=sample_size, repeats=repeats)
    present_results(results)
    print_results(results)
