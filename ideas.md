# Intro teórica
## Función de recomendación
r : [Song] -> {Song}

## Función de temática
t : [Song] -> Temática

## Lo que buscamos:
Dado p : [Song]
Queremos que t(p) =~ t(p ++ g(r(p)))

## Otro concepto:
Sea c_p, una función de criterio para una playlist p. Tenemos que
Para todo s en p, c_p(s)
Luego buscamos que para todo s2 en r(p), c_p(s2)

## Hipótesis
Cuando la gente arma playlists o escucha temas consecutivos lo hace
aplicando la función misteriosa de temática `t`
Por lo tanto, si la función `r` se hace pensando en los datasets obtenidos,
deberian lograr mantener la propiedad.
Por eso los datasets son de usuarios y no de radios donde los intereses
pueden ser distintos.


# Datasets:
*aotm 2003: muchas playlists con id de canción vinculados al msd 
    http://labrosa.ee.columbia.edu/projects/musicsim/aotm.html
*aotm 2011: muchas playlists con id de canción local
    (aproximadamente 100k playlists, y 98k canciones)
    http://bmcfee.github.io/data/aotm2011.html
*lastfm : Historial de reproducciones de usuarios (1k usuarios), vinculadas al MBDB
    http://www.dtic.upf.edu/~ocelma/MusicRecommendationDataset/lastfm-1K.html

# Métodos

* Collaborative Filtering: http://en.wikipedia.org/wiki/Collaborative_filtering
* score(c, p) = sum_p2 simil(p, p2 - {c}) * (-1 + (c in p2) * 2)


# Evaluación

## Intrínseca
*Evaluar el recomendador contra alguna función de los datos 
*Evaluar el recomendador cruzando los datastes disponibles
*Leer el paper de evaluación de generadores de playlists y tomar ideas

## Externa
*Add-on para spotify/grooveshark donde se muestren las sugerencias y se reporte que tan bien las aceptan los usuarios

## Links
* Music Similarity: http://www.ee.columbia.edu/~dpwe/research/musicsim/
    Usaron aotm 2003. Conceptos parecidos a los nuestros.
    * Aotm 2003 data: http://labrosa.ee.columbia.edu/projects/musicsim/aotm.html
