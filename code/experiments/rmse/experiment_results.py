# -*- coding: utf-8 -*-

import ml_lib.results_lib as rl
import pandas as pd
import json
import matplotlib.pyplot as plt
import numpy as np

import matplotlib
matplotlib.style.use('ggplot')
label_size = 18
matplotlib.rcParams['xtick.labelsize'] = label_size
matplotlib.rcParams['ytick.labelsize'] = label_size
matplotlib.rcParams['axes.labelsize'] = label_size


prediction_files = {
    'test': {
        'als': 'results/test_als_r30_i10_sall.result.csv',
        'ials': 'results/test_ials_r30_i10_sall.result.csv',
        'ixi': 'results/test_ixi_sall.result.csv',
        'uxu': 'results/test_uxu_sall.result.csv',
    },
    'explore': {
        'als': 'results/explore_als_r30_i10_sall.result.csv',
        'ials': 'results/explore_ials_r30_i10_sall.result.csv',
        'ixi': 'results/explore_ixi_sall.result.csv',
        'uxu': 'results/explore_uxu_sall.result.csv'
    }
}

results_per_recommender = dict(
    [(mode,
      dict([(rec_name, {}) for rec_name in files_per_rec]))
     for mode, files_per_rec in prediction_files.items()])


def predictions_desc(predictions):
    return {
        'gt1': len([r for r in predictions if r > 1]),
        'lt0': len([r for r in predictions if r < 0]),
        'total': len(predictions)
    }


def predictions_dist(rec_name, predictions, mode):
    plt.figure()
    predictions = predictions[predictions.notnull()]
    height, left = np.histogram(predictions)
    height = height / float(len(predictions))
    plt.bar(left[:-1], height, width=left[1] - left[0], log=True)
    plt.ylabel('Porcentaje de recomendaciones')
    plt.xlabel(u'puntaje recomendación')
    plt.tight_layout()
    plt.savefig('results/plots/{}_{}_pred_dist.pdf'.format(mode, rec_name))


def predictions_all_dist(name_and_preds, mode):
    name_and_preds = name_and_preds.applymap(lambda x: 1 if x > 1 else x)
    name_and_preds = name_and_preds.applymap(lambda x: 0 if x < 0 else x)
    for col_name, serie in name_and_preds.iteritems():
        predictions_dist(col_name, serie, mode)


def header_json(header):
    json_str = header[0].lstrip('# ').rstrip('\n')
    return json.loads(json_str)


def user_utilty_plot(rec_name, recs_df, m, mode,
                     xlabel='Procentaje de canciones propias recomendables'):
    plt.figure()
    user_pred = recs_df.drop('item', axis=1)
    user_pred.iloc[:, 1] = user_pred.iloc[:, 1].map(lambda x: int(x >= m))
    grouped = user_pred.groupby('user')
    agg = grouped.aggregate([np.sum, np.size])
    agg = agg[agg['pred', 'size'] >= 4]
    dist_values = agg['pred', 'sum'] / agg['pred', 'size']
    dist_values.plot(kind='hist', logy=True, normed=True)
    plt.ylabel('Porcentaje de usuarios')
    plt.xlabel(xlabel)
    plt.tight_layout()
    plt.savefig('results/plots/{}_{}_user_utility_m{}.pdf'.format(
        mode, rec_name, str(m)[2:]))

mode_config = {
    'test': {
        'utility_xlabel': 'Procentaje de canciones propias recomendables'
    },
    'explore': {
        'utility_xlabel': 'Procentaje de canciones propias recomendables'
    }
}


# Generando datos
for mode, files_per_rec in prediction_files.items():
    config = mode_config[mode]
    preds_per_recommender = {}
    for rec_name, filename in files_per_rec.items():
        hs, recs = rl.open_results(filename)
        predictions = recs.iloc[:, 2]
        results_per_recommender[mode][rec_name]['header'] = header_json(hs)
        results_per_recommender[mode][rec_name]['desc'] = predictions_desc(
            predictions)

        predictions = predictions.map(lambda x: 1 if x > 1 else x)
        predictions = predictions.map(lambda x: 0 if x < 0 else x)
        results_per_recommender[mode][rec_name]['rmse'] = rl.rmse(predictions)

        preds_per_recommender[rec_name] = predictions
        predictions_dist(rec_name, predictions, mode)

        for m in [.5, .75, .9]:
            user_utilty_plot(rec_name, recs, m, mode,
                             xlabel=config['utility_xlabel'])

    name_and_preds = pd.DataFrame.from_dict(preds_per_recommender)
    plt.figure()
    name_and_preds.plot(kind='hist', logy=True, normed=True,
                        histtype='stepfilled', subplots=(2, 2), sharex=True,
                        sharey=True)
    plt.ylabel('Porcentaje de recomendaciones')
    plt.xlabel(u'puntaje recomendación')
    plt.tight_layout()
    plt.savefig('results/plots/{}_pred_dist.pdf'.format(mode))

    # Hist pred percent
    pred_percents = [(rec_name,
                      res['desc']['total'] /
                      float(res['header']['prediction_count']))
                     for rec_name, res in results_per_recommender[mode].items()]
    pred_percents = pd.DataFrame(dict(pred_percents), index=['pred_percent'])
    pred_percents.plot(kind='bar')
    plt.tight_layout()
    plt.savefig('results/plots/{}_pred_percent.pdf'.format(mode))

with open('results/rmse.txt', 'w') as of:
    of.write('Recommender & RMSE \\\\\n')
    of.write('\hline \n')
    for rec_name, results in results_per_recommender['test'].items():
        of.write('{} & {} \\\\\n'.format(rec_name, results['rmse']))

print json.dumps(results_per_recommender, indent=4)
