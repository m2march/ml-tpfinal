
import ml_lib.experiment_lib as el

from ml_lib import cf, cases

def profile_experiment():
    sample_size = 5000

    uxu = cf.UserByUserRecommender()

    train_dict, test_dict, _ = el.split_dataset(
        cases.get_aotm2003(), sample_size)

    uxu.train(train_dict)

profile_experiment()
