import gflags
import json
import logging
import logging.config
import sys
import cPickle
try:
    import pyspark  # NOQA
except:
    pyspark = None

import ml_lib.experiment_lib as el

logger = logging.getLogger('test_prediction_experiment')


########################

gflags.DEFINE_string('setup', None, 'Experiment setup filename')

gflags.DEFINE_string('rec', None, 'Trained rec filename')

gflags.DEFINE_enum('mode', 'test', ['test', 'explore'],
                   ('Mode in which the test is run. Whether with the '
                   'test dictionary or the explore dictionary.'))

gflags.DEFINE_integer('explore_size', 50000,
                      'Item count to use when exploring')

gflags.MarkFlagAsRequired('setup')
gflags.MarkFlagAsRequired('rec')

FLAGS = gflags.FLAGS


def main(rec_file, setup_file, mode, explore_size, spark_context):
    logger.info('Predictions in mode %s for rec %s and setup %s.',
                mode if mode == 'test' else '{}({})'.format(mode, explore_size),
                rec_file, setup_file)
    trained_rec = el.load_recommender(rec_file, spark_context)
    with open(setup_file, 'r') as sf:
        setup = cPickle.load(sf)

        if trained_rec.setup_id != setup.id:
            i = input('Recommender was not trained with this setup. '
                      'Continue? (Y/n)')
            if i != 'Y':
                sys.exit(1)

        if mode == 'test':
            predict_dict = setup.test_dict
        else:
            predict_dict = setup.explore_dict(explore_size)

        predictions = el.predict(trained_rec, predict_dict)
        results = el.ExperimentResult(setup, trained_rec, mode,
                                      predict_dict, predictions)

        outfile_name = 'results/{}_{}_s{}.result.csv'.format(
            mode, trained_rec, setup.sample_size)
        with open(outfile_name, 'w') as of:
            header = results.json_header()
            of.write('# {} \n'.format(json.dumps(header)))
            of.write('\n'.join(
                ['# ' + l for l in json.dumps(header, indent=4).split('\n')]
                + [''])
            )
            of.write('# user_id, item_id, prediction_rating\n')
            for prediction in predictions:
                of.write('{}, {}, {}\n'.format(*prediction))
        logger.info('Predictions (%d) stored in: %s',
                    len(predictions), outfile_name)


if __name__ == '__main__':
    try:
        argv = FLAGS(sys.argv)  # parse flags
    except gflags.FlagsError, e:
        print '%s\\nUsage: %s ARGS\\n%s' % (e, sys.argv[0], FLAGS)
        sys.exit(1)

    logging.config.dictConfig({
        'version': 1,
        'formatters': {
            'basic': {
                'format': '%(asctime)s %(levelname)s %(name)s :: %(message)s'
            }
        },
        'handlers': {
            'console': {
                'class': 'logging.StreamHandler',
                'stream': 'ext://sys.stdout',
                'formatter': 'basic',
                'level': 'INFO'
            },
            'log_file': {
                'class': 'logging.FileHandler',
                'filename': 'test_prediction_experiment.log',
                'formatter': 'basic',
                'level': 'DEBUG'
            }
        },
        'loggers': {
            'ALSRecommenderAdapter': {
                'level': 'DEBUG',
                'handlers': ['console', 'log_file'],
                'formatter': 'basic'
            },
            'ItemByItemRecommender': {
                'level': 'DEBUG',
                'handlers': ['console', 'log_file'],
                'formatter': 'basic'
            },
            'UserByUserRecommender': {
                'level': 'DEBUG',
                'handlers': ['console', 'log_file'],
                'formatter': 'basic'
            }
        },
        'root': {
            'level': logging.DEBUG
        }
    })

    if pyspark:
        spark_conf = (pyspark.SparkConf()
                      .setAppName('Prediction Experiment')
                      )
        spark_context = pyspark.SparkContext(conf=spark_conf)
    else:
        spark_context = None

    main(FLAGS.rec, FLAGS.setup, FLAGS.mode, FLAGS.explore_size, spark_context)
