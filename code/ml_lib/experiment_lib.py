import numpy as np
import shutil
import os
import cPickle
import random
import math
import logging
import cases
import uuid
import cf

try:
    import pyspark
    from pyspark.mllib.recommendation import MatrixFactorizationModel
    import als
except:
    pyspark = None
    als = None


logger = logging.getLogger('experiment_lib')


dataset_funcs = {
    'aotm2003': cases.get_aotm2003
}


def split_dataset(u_i_dict, sample_size, test_percent=0.2):
    """
    Splits the dataset into train and test dictionaries.
    To split the dataset, de user-item dictionary is split into two dictionaries
    with the same keys as the original but with the values split in the given
    reason.

    Args:
        u_i_dict: user_id (playlist) -> [item_id] (songs)
        sample_size: number of users to use from u_i_dict or keyword 'all'
        test_percent: ideal percentage of train items split as test

    Returns:
        (train_dict, test_dict, actual percentage of total items in test)
    """
    if sample_size == 'all':
        sample_size = len(u_i_dict)

    sample_r_dict = dict(random.sample(u_i_dict.items(),
                                       min(int(sample_size), len(u_i_dict))))
    test_dict = {}
    train_dict = {}
    for user_id, items_ids in sample_r_dict.items():
        random.shuffle(items_ids)
        slice_idx = int(math.floor(len(items_ids) * test_percent))
        test_dict[user_id] = items_ids[:slice_idx]
        train_dict[user_id] = items_ids[slice_idx:]

    total_items = sum([len(v) for v in sample_r_dict.values()])
    test_items = sum([len(v) for v in test_dict.values()])
    test_percent = float(test_items) / total_items
    return (sample_r_dict, train_dict, test_dict, test_percent)


def normed_hist(values, bins):
    hist = np.histogram(values, bins=bins)[0]
    logger.debug('normed_hist :: #: %s h: %s s: %s b: %s, v: %s',
                 len(values), str(hist), hist.sum(),
                 bins, values)
    return hist / float(hist.sum())


def read_predictions_from_lines(lines):
    def parse_lines(lines):
        for l in lines:
            if not l.startswith('#'):
                s = l.split(',')
                yield (int(s[0]), int(s[1]), float(s[2]))
    return list(parse_lines(lines))


class ExperimentSetup():

    def __init__(self, dataset_name, sample_size, test_percent=0.2,
                 explore_set_size=10000):
        u_i_dict = dataset_funcs[dataset_name]()
        (self.sample_r_dict,
         self.train_dict,
         self.test_dict,
         self.split_percent) = split_dataset(u_i_dict, sample_size,
                                             test_percent)
        self.sample_size = sample_size
        self.id = uuid.uuid4()
        all_items = [i
                     for items in self.sample_r_dict.values()
                     for i in items]
        self.explore_items = random.sample(all_items, explore_set_size)

    def explore_dict(self, explore_size):
        ex_items = self.explore_items[:explore_size]
        return dict([(user_id, ex_items)
                     for user_id in self.train_dict.keys()])

    def desc(self):
        return {
            'id': str(self.id),
            'sample_size': self.sample_size,
            'total_train_items': sum([len(items)
                                      for items in self.train_dict.values()]),
            'total_test_predictions': sum(
                [len(items) for items in self.test_dict.values()]),
            'total_explore_items': len(self.explore_items)
        }


class ExperimentResult():

    def __init__(self, setup, recommender, experiment_type, predict_dict,
                 predictions):
        self.setup_desc = setup.desc()
        self.recommender_desc = recommender.desc()
        self.experiment_type = experiment_type
        self.prediction_count = sum([len(vs)
                                     for vs in predict_dict.values()])
        self.predictions = predictions

    def json_header(self):
        header = dict()
        header.update(self.setup_desc)
        header.update(self.recommender_desc)
        header.update({'experiment_type': self.experiment_type,
                       'prediction_count': self.prediction_count})
        return header


def predict(trained_rec, predict_dict):
    '''
    Preforms predictions on the prediction dictionary.

    Returns:
        :: [(user_id, item_id, prediction)]
    '''

    user_item_ids = [(user_id, item_id)
                     for user_id, item_ids in predict_dict.items()
                     for item_id in item_ids]

    logger.info('Performing predictions for recommender %s '
                'with %d users and total %d predictions',
                str(trained_rec), len(predict_dict), len(user_item_ids))

    predictions = trained_rec.predictAll(user_item_ids)

    return predictions


def save_recommender(trained_rec, basename):
    full_path = None
    if (als is not None and isinstance(trained_rec, als.ALSRecommenderAdapter)):
        full_path = basename + '.mfm'
        if os.path.exists(full_path):
            shutil.rmtree(full_path)
        trained_rec.model.save(trained_rec.spark_context, full_path)
        metadata_path = os.path.join(full_path, 'model.meta.pkl')
        with open(metadata_path, 'w') as of:
            cPickle.dump(trained_rec.metadata(), of)
    elif (isinstance(trained_rec, cf.Recommender)):
        full_path = basename + '.pkl'
        with open(full_path, 'w') as of:
            cPickle.dump(trained_rec, of)
    else:
        raise ValueError('Saving recommender of unknown type: %s' %
                         trained_rec.__class__.__name__)
    return full_path


def load_recommender(path, spark_context):
    if path.endswith('.mfm'):
        model = MatrixFactorizationModel.load(spark_context, path)
        metadata_path = os.path.join(path, 'model.meta.pkl')
        with open(metadata_path, 'r') as f:
            metadata = cPickle.load(f)
            return als.ROALSRecommenderAdapter(model, metadata, spark_context)
    elif path.endswith('.pkl'):
        with open(path, 'r') as f:
            return cPickle.load(f)
    else:
        raise ValueError('Unexpected extension for model file %s' %
                         path)
