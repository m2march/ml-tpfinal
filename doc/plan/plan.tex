\documentclass[a4paper]{article}

\usepackage[utf8]{inputenc}
\usepackage[spanish]{babel}
\usepackage{url}
\usepackage{footnote}
\usepackage{hyperref}
\usepackage{todonotes}

\title{Plan del trabajo práctico final de Aprendizaje Automático \\
\textsf{\large Recomendación de canciones para listas de reproducción} }
\author{Matías Incem y Martín Miguel \\     
{\small matiasincem@gmail.com; m2.march@gmail.com}}


\begin{document}

\maketitle

\section{Introducción}

El objetivo del presente trabajo es investigar la posibilidad de desarrollar un
sistema de recomendación de canciones que cumplan con un criterio intrínseco de
una lista de reproducción. Esto es, considerando que las canciones fueron
agrupadas dentro de una lista dado un cierto criterio que cumplen lo
suficientemente bien, el sistema buscará inferir el criterio para buscar otras
canciones que también lo cumplan.

La mayoría de los sistemas comerciales existentes realizan recomendaciones
relacionadas a un usuario en su globalidad. Esto es, en general no tienen
en cuenta aspectos locales sobre el tipo de música que desea escuchar en ese
momento; y si lo hacen difieren del mencionado anteriormente.
\textbf{Last.fm}\footnote{\url{www.last.fm}} realiza recomendaciones a un
usuario de bandas y canciones por funciones de cercanía en base a sus colección
de tags obtenidas por input de los usuarios.
\textbf{Grooveshark}\footnote{\url{www.grooveshark.com}} realiza
recomendaciones al usuario recopilando cosas que escuchó anteriormente y por
preferencia de géneros, usando los temas más populares por género.
\textbf{Pandora}\footnote{\url{www.pandora.com}} es uno de los sistemas que
utiliza localidad ya que genera las denominadas \emph{estaciones} que producen
recomendaciones a partir de palabras claves, como son el nombre de una banda,
el nombre de una canción, el nombre de un género o algunos conceptos abstractos
como 'navidad' o 'roadtrip'. Pandora utiliza características acusticas de las
canciones que fueron anotadas manualmente.
\textbf{Spotify}\footnote{\url{www.spotify.com}} genera recomendaciones de
acuerdo al perfíl de gustos del usuario (música similar a la que escucha), por
recomendaciones sociales (lo que escuchan los amigos del usuario) y posee una
funcionalidad de 'radio' que realiza lo que estamos buscando. La funcionalidad
de 'radio' genera una radio \textemdash una selección contínua de temas
\textemdash a partir de una banda, canción o lista de reproducción. Cuando esto
se probó con algunas listas de reproducción, las canciones sugeridas por
Spotify no se consideró que mantuvieran el espíritu de la lista.

\subsection{El problema}

El recomendador a construir puede considerarse una función $r$ tal que:

$$ r_p \approx c_p $$

con $c_p : s \rightarrow [0,1] \subseteq \mathcal{R}$ el criterio intrínseco de
una lista de reproducción $p$ para distintas canciones $s$.

Considerando que este criterio a aproximar depende principalmente de las
canciones de la lista de reproducción y funciona agrupando las mismas dadas
ciertas características que estas comparten, utilizaremos las mismas para
tratar de comprender cuál es el criterio.  La idea es entonces buscar otra
lísta de reproducción $p$ que comparta varias canciones con la lista activa
$p_a$. Luego se consideran como recomendaciones las canciones que la primera tiene y
la segunda no. Estas recomendaciones serían potencialmente buenas ya que si 
el criterio de $p_a$ elije las canciones por característisticas en común y $p$
posee varias de esas canciones, es probable que las otras canciones en $p$
también posean estas características. En particular buscamos evitar sistemas de
recomendación que se basen en \emph{datos editoriales}\cite{song_asurvey} como
son género y artista, ya que para que esto funcione debe suceder que el género
y el artista agrupen solo canciones que cumplen con el criterio.
\todo{revisar}


Este concepto para sistemas de recomendación es también conocido como
\textsf{Collaborative
filtering}\footnote{\url{http://en.wikipedia.org/wiki/Collaborative_filtering}}.
En \textsf{collaborative filtering}, se tiene una matriz de opiniones
(\emph{reviews}) $R$ de dimensiones $|U| \times |I|$ donde $U$ es el conjunto
de usuarios e $I$ es el conjunto de items. Las opiniones pueden ser numéricas,
o categóricas. $r_{i,j}$ representa la opinión del usuario $i$ sobre el item
$j$. La matríz tendrá un valor especial en su dominio que representa el caso
donde no hay opinión del usuario para el item \cite{rec_sys_eml}.

El problema aquí planteado puede modelarse como un caso de CF, donde los
usuarios son las listas de reproducción y los items son las canciones. Los
elementos en R poseen dos valores: 0 y 1. El 0 indica que la canción no está en
la lista y por lo tanto no sabemos sobre la opinión de la lista respecto de la
canción. El 1 indica que la canción se encuentra en la lista y luego la lista
'opina' positivamente sobre la misma. Esta opinión está modelada por la función
$c_p$. Sabemos entonces que el recomendador $r$ construído con CF va a cumplir
el criterio buscado.

\section{Plan}

En el trabajo aquí descripto se buscarán evaluar distintos sistemas de
recomendación que permitan modelar la función $r$. 

\subsection{Métodos}

Los métodos a evaluar son:

\begin{enumerate}
    \item User by user memory-based CF\label{method:uxu-cf}
    \item Item by item memory-based CF\label{method:ixi-cf}
    \item User by user memory-based CF con reducción de
        dimensionalidad\label{method:uxu-cf-svd}
    \item Content-based recommender usando tags\label{method:content-tags}
\end{enumerate}

El método \ref{method:uxu-cf} es la implementación más común de CF orientado a
usuarios, donde $r_{p_a}(s)$ se estima como un promedio pesado de $r_p(s)$ por
la distancia de $p$ a $p_a$ bajo algún criterio. Por las características del
sistema esta distancia será la \emph{distancia
Jaccard}\footnote{\url{http://en.wikipedia.org/wiki/Jaccard_index}}. Este
método posee el problema comocido como \emph{data sparcity}. La capacidad del
sistema depende de encontrar usuarios parecidos al usuario activo. Las listas
de reproducción tienen una duración mucho menor a la cantidad de canciones que
se manejan en un sistema, por lo que a veces es difícil armar un buen
vecindario alrededor del usuario activo.

El método \ref{method:ixi-cf} presenta otra implementación clásica de CF donde
el usuario activo recibe recomendaciones de items porque es parecen a los que
ya eligió. La diferencia con otros sistemas de recomendación que recomiendan
items parecidos es que en CF la similitud se calcula a partir de lo que opinan
los usuarios. Esto mantiene el criterio de recomendación que buscamos. Con este
método alternativo se pretende buscar una posible solución al problema de
\emph{data sparcity}.

El método \ref{method:uxu-cf-svd} presenta una modificación para el método
\ref{method:uxu-cf} reduciendo la dimensionalidad de la matríz $R$. Esta es una
técnica común para resolver el problema de \emph{data
sparcity} \cite{su2009survey}. Para reducir la dimensionalidad se propone
utilizar
SVD\footnote{\url{http://en.wikipedia.org/wiki/Singular_value_decomposition}}.

Finalmente el método \ref{method:content-tags} se aleja de CF y busca
representar el criterio $c_p$ utilizando tags de las canciones y entrenando un
recomendador para cada lista de reproducción, ajustando la importancia de cada
tag en la recomendación según la lista activa.


\subsection{Datos}

Los datos a utilizar son dos datasets de listas de reproducción denominados
\textbf{aotm2003}\cite{aotm2003} y \textbf{aotm2011}\cite{aotm2011}.
\textbf{Art of the mix}\footnote{\url{www.artofthemix.org}} (\emph{aotm}) es un 
sitio social donde se coleccionan listas de reproducción. Muchas son obtenidas
a partir de las listas creadas en \emph{iTunes}. Los datasets nombrados
anteriormente fueron generados a partir del sitio web.

\subsection{Criterio de evaluación}

Los criterios de evaluación clásicos para sistemas de recomendación y CF en
particular son
RMSE\footnote{\url{http://en.wikipedia.org/wiki/Root-mean-square_deviation}} y
ROC\footnote{\url{http://en.wikipedia.org/wiki/Receiver_operating_characteristic}}.
En RMSE, el error es la diferencia de la predicción de la preferencia del
usuario $u$ al item $i$ con la preferencia real. El ROC es un índice de sobre
si las recomendaciones fueron aceptadas por el usuario y las no-recomendaciones
no eran deseadas. 

A el problema en particular que aquí presentamos no pueden aplicarse ningunas
de estas dos métricas sin modificaciones. En nuestro caso, los usuarios (las
listas de reproducción) tienen una opinión binaria respecto de los items (las
canciones).  Esta opinión es o positiva o indiferente. La opinión positiva
ocurre cuando la lista contiene a la canción; pero si la lista no
contiene a la canción no podemos decir que su opinión es negativa. Esto
dificulta el resultado del RMSE ya que el error es o 0 o 1 y la métrica pierde
su poder descriptivo. En cuando al ROC, se presenta un problema como
consecuencia de la falta de opiniones negativas y la esparcidad de la matriz de
opiniones. Esto significa muy pocos 1s por fila y muchisimos 0s, lo que hace que
valores como \emph{precisión} y \emph{false-positive rate} estén muy sezgados al
0 cuando los 0s de la matriz se consideran opiniones negativas. Si los 0s se
consideran que no emiten opinión, los índices no pueden calcularse.

Como alternativa presentamos el siguiente criterio de evaluación, que otorga un
índice de utilidad al recomendador para un usuario. La utilidad de un
recomendador $r$ para un usuario $u$ es:

$$
        util_r(u) = \frac{1}{|u|} \sum_{i \in u} r_{u - \{i\}}(i)
$$

Considerando a $r$ una función que dado un usuario (lista de reproducción) da
un valor entre 0 y 1 que representa la probabilidad de pertenencia a la lista
o, visto de otra manera, la concordancia con el criterio intrínseco de la
lista. La evaluación de utilidad se hace viendo qué correspondencia le da el
sistema a una canción que sabemos cumple con el criterio si no estuviera en la
lista, y luego promediando sobre estos valores. 

Luego se puede ver la distribución de la utilidad del recomendación sobre el
conjunto de usuarios del set de prueba. Ver la distribución es intersante ya
que es razonable que ciertos usuarios no sean fáciles de recomendar bajo este
criterio. Este problema se conoce como \emph{black sheep} y \emph{grey
sheep}\cite{su2009survey}: cuando el usuario tiene opiniones muy particulares y
cuando el usuario no se alínea fuertemente con ningún grupo y por lo tanto las
recomendaciones poseen menos confianza, respectivamente. Consideramos que en el
caso de las listas de recomendación, esto es probable que suceda.

El criterio de utilidad presentado anteriormente posee como falla que el
recomendador óptimo es aquél que otorga un puntaje de 1 a todos los items
independientemente del usuario. Esto es porque no tenemos opiniones negativas
en las cuales el recomendador deba dar un bajo puntaje. No obstante, un
recomendador que recomiende todo es de poca utilidad. Para evaluar esto se
propone contabilizar la cantidad de canciones que no se encuentran en la lista 
sobre las cuales el recomedador da un puntaje más alto que el otorgado a las
canciones de la lista. Definitivamente sabemos que las canciones que se
encuentran en la lista cumple con el criterio mejor que la mayoría de las
canciones que no lo hacen - por algo están en la lista. Luego, los casos donde
la recomendación es más fuerte para una canción que no está en la lista
deberían ser pocos. 


\bibliographystyle{plain}
\bibliography{references}

\end{document}
