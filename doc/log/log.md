# Sacando numeros
_2015/12/05_

## Dudas

* Revisar porqué ials y als tienen valores por fuera del rango [0, 1]. ¿Hay
    problemas de configuración? ¿Habrá que interpretarlos distinto?
    Por ahora se elimina la saturación de ambos lados
* Revisar porqué uxu tiene valores fuera de rango (> 1)

## Gráficos

* Ver de mantener la escala entre gráficos de distribución de predicciones
* Emprolijar el pred_percent

## Informe

* Buscar porque ALS decide no devovler recomendaciones para algunas tuplas
    (user, item)


## Resultados

* Tenemos RMSE global como métrica de error. Poco interesante pero es una
    primera herramienta. (Posiblemente se puede ver de comprar con RMSEs de
    la bibliografía (netflix prize?))

* Tenemos distribución de predicciones del test set para ver comportamiento
    global (results/plots/*_pred_dist.pdf). Lo principal para ver acá es qué
    parte de los resultados están cerca del cero y que parte están en el 1.
    Esto se va a relacionar bien con el RMSE.

* Tenemos el dato de que porcentaje de las prediccions del test set se pudieron
    realmente hacer en cada sistema. Esto es porque los sistemas, para algunos
    casos no puede decidir en ningúna predicción del rating. En als en
    particular no sabemos porqué. En ixi y uxu está en el código (cf.py).

* El gráfico de la sección 6.4 nos sirve como métrica para ver la distribución
    de la utilidad por usuario. La utilidad la medimos como cuantas canciones
    el sistema le recomienda según M.

* El gráfico de procentaje se puede sacar para los datos por fuera de testing
    (para hacer)


# Evaluación

## RMSE

En principio calculamos RMSE en general. Esto nos da entre 0,007 y 0,010 (varía según algoritmo), que nos dice qué tan bueno es cada algoritmo para recomendar a una playlist las canciones que esta tenía originalmente (pero que se las sacamos para testear).
Esto es lo único que podemos hacer con el feedback que tenemos. Si agarramos una canción y una playlist arbitrarias, no tenemos idea de si la canción es buena recomendación para esa playlist o no, a menos que le preguntemos a la gente.
Dado ese resultado, podemos calcular RMSE para cada usuario. Esto nos permite saber si el algoritmo es igual de bueno para todos los usuarios o es muy bueno para algunos y malo para otros.

__Problema del RMSE__: si un algoritmo les da puntaje 1 a todas las canciones, su RMSE es perfecto. No queremos evaluar este recomendador como bueno, porque es trivial y recomienda todo a todo. Un buen recomendador debería dar puntaje bajo a las canciones que no son apropiadas para la playlist. Entonces necesitamos otra medida de evaluación además del RMSE.

## Canciones según puntaje

Nos fijamos, para cada playlist, qué porcentaje de canciones le son recomendadas con cada valor entre 0 y 1 (tomamos intervalos para mostrar esto). Hacemos el promedio o la sumatoria entre todas las playlists y graficamos.
(eje x = puntaje ; eje y = cantidad de canciones)
En el item x item, nos da una gran mayoría de canciones con puntaje 0, en general. Esto pasa porque, para que nos dé un puntaje mayor que 0, tiene que haber una playlist que tenga un item de la playlist actual, y el item actual a recomendar, al mismo tiempo. Esto pasa muy poco en nuestro dataset.
Podemos ver los datos eliminando los ceros, y se aprecian mejor.

## Playlists según cantidad de canciones recomendadas

Tomemos un valor M entre 0 y 1, tal que un puntaje mayor o igual a M es una buena recomendación. (Ej: M = 0,5 ; las canciones puntuadas mejor que 0,5 son recomendables.)

Queremos graficar las playlists del dataset según la cantidad de canciones recomendables que podemos ofrecerles.
(eje x = cant. canciones recomendables ; eje y = cant. playlists)

Podemos medir esto sin contar las canciones que sacamos para testear. Es decir, solo recomendar canciones que nunca fueron parte de la playlist.

# Explorando recomendadores

## ALS

### Parametro: iteraciones
'ALS r(30) i(5)': [0.010520506364530989], 
'ALS r(30) i(10)': [0.0087683798595562055], 
'ALS r(30) i(15)': [0.0068232124395305998]}
'ALS r(30) i(30)': [0.002980110666145373], 


# Implementando ALS para nuestros experimentos
_2015/10/10_

## Con Spark

[Spark Python API doc](http://spark.apache.org/docs/latest/api/python/pyspark.mllib.html#module-pyspark.mllib.recommendation)

# Armando el proyecto 
_2015/08/12_

## Definición del proyecto:

El objetivo del trabajo es lograr una evaluacióm completa de distintos métodos
para el problema de recomendar canciones a listas de reproducción. El problema
entra en la disciplina de Recommender Systems, pero posee varias
particularidades (sparcidad, implicito, falta de feedback negativo, feedback
positivo unario).

Vamos a evaluar 3 modelos (y sus parametrizaciones) para el problema de
recomendar canciones para listas de reproducción. La evaluación se hace con los
datasets utilizando cross-validation. Esto significa, en repetidas
instancias se quitan subconjuntos de las opiniones de usuario a item y luego se
ve que tan bien se recuperan estos valores (RMSE contra valor esperado 1).
Además, dado que queremos evitar la solución trivial de recomendar todo a todo,
buscaríamos una métrica para ver coherencia de los resultados fuera del
dataset. Una alternativa es evaluar, para cada usuario, cuantos casos hay donde
canciones que no están en la playlist se recomiendan más que las que si están.

Los modelos son user-based collaborative filtering, item-based collaborative
filtering y ALS. Los tres serán implementados en python dada su sencillez
(salvo que veamos que es muy complicado). Los métodos de vecindario (user-based
e item-based) están ajustados para el problema particular en cuestión
(distancia de Jaccard). ALS entendemos que funciona de forma genérica. 

Además nos interesa analizar más en profundidad la utilidad real de los
métodos. Por ejemplo, algunos de los métodos pueden ser muy buenos para algunos
usuarios en particular, a pesar de ser malo para la mayoría. Alternativamente,
el método podríá ser decente para todos pero no destacarse para ningún tipo de
usuario. Esto surge del hecho de que no todas las playlist son coherentes o
igual de fáciles de recomendar (hipótesis que se mantiene en otros sistemas -
películas). Para ver esto proponemos evaluar la utilidad del modelo para cada
usuario y analizar la distribución de esta utilidad. Esto puede implicar hacer
la separación del dataset en training/test de forma que todos los usuarios se
vean reducidos en información de forma equitativa.

Finalmente sería razonable hacer una breve evaluación de las recomendaciones
por fuera del dataset, esto es, qué recomienda el modelo a las playlist por
fuera de ellas. Esto deberá hacerse a mano con oyentes reales. Ej.: el modelo
recomienda 5 canciones para algunos usuarios selectos y los oyentes otorgan
puntos según el 'matching' de la canción con la lista de reproducción. Luego se
debe comparar para los distintos modelos, que puntaje en promedio tuvieron las
canciones recomendadas.

## Ideas para el informe
* Agregar discusión sobre el tipo de feedback en el informe (implicito vs.
    explicito). Problema de la solución trivial. 
* Investigar como la reducción de dimensionalidad sirve para factores latentes

Website:

* [WEEK 7: HUNCH.COM, RECOMMENDATION ENGINES, SVD, ALTERNATING LEAST SQUARES, CONVEXITY, FILTER BUBBLES](http://columbiadatascience.com/2012/10/18/week-7-hunch-com-recommendation-engines-svd-alternating-least-squares-convexity-filter-bubbles/)
* [Página del paper original de ALS para _implicit feedback_](http://bugra.github.io/work/notes/2014-04-19/alternating-least-squares-method-for-collaborative-filtering/)
* [ALS better explained](http://www.slideshare.net/srowen/big-practical-recommendations-with-alternating-least-squares)

# Findings
_2015/03/24_

* http://lenskit.org/

# Consdieraciones
_2015/03/18_

* UxU tarda mucho, pensamos en precalcular las similitudes de usuario y luego
    usar un vecindario y recalcular solo esos.

* En IxI puede revisarse la distancia entre items (miedo a que se aplane las
    dimensiones de la canción).
* En IxI también se complica la métrica de predicción. Dividimos por la
    cantidad de canciones en el usuario pero podría ser otra normalización.

## ALS:

* Se habla mucho de _factores_, averiguar que son.

Websites:

* [Página del paper original de ALS para _implicit feedback_](http://bugra.github.io/work/notes/2014-04-19/alternating-least-squares-method-for-collaborative-filtering/)
* [Implementación de ALS-WR en Apache Spark](http://spark.apache.org/docs/1.2.1/mllib-collaborative-filtering.html)

# La problemática de los datos 2 (y posibles soluciones) 
_2014/12/15_

Revisando el artículo **A Survey of Collaborative Filtering Techniques**
(`doc/421425.pdf`), se describe el problema de la falta de datos como un
problema usual en el mundo de CF. El problema es conocido como _Data Sparcity_.
En la revisión de métodos de CF se presentan distintas soluciones para este
problema. Además el artículo presenta otras dificultades que suelen
presentarse en CF. A continuación explicamos como afectan cada uno al problema
a trabajar en nuestro trabajo:

* _Data Sparcity_: Efectivamente un problema en nuestro caso. La matríz item x
    usuarios a trabajar tiene las canciones como items, las playlists como
    usuarios y una única categoría de review. La cantidad de posiciones en la
    matriz con datos es muy baja.
* _Scalability_: No aplica a los fines del trabajo ya que nuestro único interés
    en la velocidad es que las técnicas sean ejecutables en un tiempo razonable
    para nuestros datos de prueba.
* _Synonymy_: No aplica a nuestro problema ya que las canciones fueron
    correctamente identificadas en los datasets a utilizar. 
* _Gray Sheep_: Podría ser
* _Shilling Attacks_: No es de interés
* _Noise_: Podría considerarse que no aplica ya que puede hipotetizarse que las
    personas no se equivocan en qué canciones hay en una playlist que armaron.
    De suceder, es razonable considerar que el número de casos será
    despreciable.
* _Explainability_: No es de principal interés

A continuación listamos las posibles tecnicas a considerar en el trabajo.

## Memory-based CF

* _User by user memory-based CF_: Para el cálculo de similitud consideramos que 
    la correlacion de Pearson no es particularmente útil ya que tenemos 
    categorías binarias. En cambio proponemos utilizar el _índice Jaccard_[^J].
    Para computar la predicción utilizamos una simple suma pesada.

    Este método sufre extremádamente por la falta de datos. ~~Estaría faltando
    encontrar una buena métrica para ver cuanto lo afecta.~~

[^J]: <http://en.wikipedia.org/wiki/Jaccard_index>

* _Item by item memory-based CF_: En lugar de predecir el valor de una canción
    en una lista según el juicio de usuarios similares puede predecirse el
    valor de un item según su parecido a los items en la playlist objetivo.
    La similitud de items se calcula de la misma matriz _usuario x item_,
    por lo que conservaría la propiedad de respetar el criterio inherente a
    la lista de reproducción. 
    
    Queda pendiente evaluar el efecto de la escasez de datos sobre el
    método. 

* _Dimensionality reduction_: El artículo propone la reducción de
    dimensionalidad del espacio de la matríz para resolver el problema de
    escasez de datos. Esta técnica es genérica y no ajustada al problema. Puede
    evaluarse a modo de referencia. Un método propuesto es el de
    SVD[^SVD].

    Queda pendiente entender como implementarlo.

[^SVD]: <http://en.wikipedia.org/wiki/Singular_value_decomposition>

### Fine tunings

* _Inverse User Frequency_: Desvalora el efecto de los items que son muy
    popualares entre muchos usuarios considerando que tienen poco valor
    predictivo. Esto puede llegar a aplicarse al problema de completar
    playlists.

* _Case Amplification_: Aumenta el efecto de los buenos rankings en
    comparación con aquellos de menor valor.

## Model-based CF

* _Advanced Bayesian Belief Networks_: En el artículo se mencionan estos
    métodos y se habla bien de su capacidad para resolver el problema de la
    escasez de datos. No obstante la información de su funcionamiento es poa y
    no parece tarea fácil implementarlos.

* _Regression-based CF_: No parecería particularmente apropiado ya que los
    rankings trabajados no son numéricos, pero podrían generar ideas
    interesantes para modelos que aprovechen mejor los pocos datos que
    poseemos. Existe un problema en sí que es que solo sabmeos si la canción
    pertenece o no al criterio contenido por la lista de reproducción. Para la
    mayoría de los algoritmos de recomendación esto es poca información.

* _Latent Semantic CF Models_: Estos modelos buscan explicar características de
    las variables vistas (ratings para items por usuarios) bajo una o más
    variables de clase que agrupan a los elementos. Aunque no de forma obvia,
    es posible que estos modelos sirvan para representar el _concepto_ latente
    que se considera define a una playlist.

## Non-CF recommenders

* _Reglas de asociación_: Las reglas de asociación no se consideran CF, pero
    suelen ser utilizadas para la misma tarea de top-N recommendation. En el
    artículo se nombran autores donde el mecanismo se aplica a la tarea.

* _Content-based recommender_: Alternativamente a recomendadores con CF puro,
    se puede incluir el conjunto de tags por canción de **Last.fm**[^L]
    utilizado por McFee [PlaylistMcFee]. A partir de las canciones en una
    playlist puede entonces obtenerse un perfíl de tags asociados a la playlist
    y su relevancia para que una canción pertenezca a la misma y utilizar esto
    como recomendación.  Además, podría usarse un vecindario del usuario para
    extender el conjunto de tags a evaluar. 

[^L]: <http://labrosa.ee.columbia.edu/millionsong/lastfm>

## Criterio de evaluación

Los criterios de evaluación clásicos para sistemas de recomendación y CF en
general son RMSE[^RMSE] y ROC[^ROC]. En RMSE, el error es la diferencia de la
predicción de la preferencia del usuario _u_ al item _i_ con la preferencia
real. El ROC es un índice de sobre si las recomendaciones fueron aceptadas por
el usuario y las no-recomendaciones no eran deseadas. 

[^RMSE]: <http://en.wikipedia.org/wiki/Root-mean-square_deviation>
[^ROC]: <http://en.wikipedia.org/wiki/Receiver_operating_characteristic>

A el problema en particular no pueden aplicarse ningunas de estas dos métricas
sin modificaciones que tengan en cuenta algunas características del mismo. En
nuestro caso, los usuarios (las listas de reproducción) tienen una opinión
binaria respecto de los items (las canciones). Esta opinión es o positiva o
indiferente. La opinión positiva ocurre cuando la lista contiene a la canción.
Si la lista no contiene a la canción no podemos decir que su opinión es
negativa. Esto dificulta el resultado del RMSE ya que el error es o 0 o 1 y la
métrica pierde su poder descriptivo. En cuando al ROC, se presenta un problema
como consecuencia de la falta de opiniones negativas y la esparcidad de la
matriz de opiniones. Las listas de reproducciones suelen tener en promedio 20
canciones, en un conjunto del orden de 10000 canciones. Esto significa muy
pocos 1s por filas y muchisimos 0s. Esto hace que valores como _precisión_ y
_false-positive rate_ estén muy sezgado al 0 si los 0s de la matriz se
consideran opiniones negativas; o no puedan calcularse si los 0s se consideran
que no emiten opinión. 

Como alternativa presentamos el siguiente criterio de evaluación, que otorga un
índice de utilidad al recomendador para un usuario. La utilidad de un
recomendador `r` para un usuario `u` es:

$$
    util_r(u) = \frac{1}{|u|} \sum_{i \in u} r(u - {i}, i)
$$

Considerando a `r` una función que dado un usuario (lista de reproducción) da
un valor entre 0 y 1 que representa la probabilidad de pertenencia a la lista
o, visto de otra manera, la concordancia con el criterio intrínseco de la
lista. La evaluación de utilidad se hace viendo qué correspondencia le da el
sistema a una canción que sabemos cumple con el criterio si no estuviera en la
lista, y luego promediando sobre estos valores. 

Luego se puede ver la distribución de la utilidad del recomendación sobre el
conjunto de usuarios del set de prueba. Ver la distribución es intersante ya
que es razonable que ciertos usuarios no sean fáciles de recomendar bajo este
criterio. Este problema se conoce como _black sheep_ y _grey sheep_ cuando el
usuario tiene opiniones muy particulares y cuando el usuario no se alínea
fuertemente con ningún grupo y por lo tanto las recomendaciones poseen menos
confianza, respectivamente. Consideramos que en el caso de las listas de
recomendación, esto es probable que suceda.


El criterio de utilidad presentado anteriormente posee como falla que el
recomendador óptimo es aquél que otorga un puntaje de 1 a todos los items
independientemente del usuario. Esto es porque no tenemos opiniones negativas
en las cuales el recomendador deba dar un bajo puntaje. No obstante, un
recomendador que recomiende todo es de poca utilidad. Para evaluar esto se
propone contabilizar la cantidad de canciones que no se encuentran en la lista 
sobre las cuales el recomedador da un puntaje más alto que el otorgado a las
canciones de la lista. Definitivamente sabemos que las canciones que se
encuentran en la lista cumple con el criterio mejor que la mayoría de las
canciones que no lo hacen - por algo están en la lista. Luego, los casos donde
la recomendación es más fuerte para una canción que no está en la lista
deberían ser pocos. 

# La problemática de los datos
_2014/11/26_

En nuestra visión del sistema de recomendación, estamos considerando a las
lístas de reproducción como usuarios en el mecanismo de recomendación
conocido como **collaborative filtering** [Recommender Systems Handbook p.107, 
Encyclopedia of Machine Learning ch.338 p.1]. El problema es que, en nuestros
juegos de datos (aotm 2003 y aotm 2011), existe poca intersección
entre los usuarios, por lo que es difícil recomendar a un ítem a un usuario 'u'
si hay pocos usuarios 'v' similares a 'u'. Para evaluar esta similitud 
evaluamos la intersección de items por usuarios. Resulta que son muy pocos los
usuarios que tienen algún otro usuario con muchos items en común.

No obstante, uno asumiría que siendo yo un usuario 'u' (una playlist), 
las canciones que contengo deberían estar en algúna otra playlist,
individualmente, y potencialmente esa playlist podría compartir más canciones
conmigo. A medida que tiene más canciones, es más razonable que considere
sus recomendaciones. Sería necesario ver entonces para cada canción, a cuantas
playlist pertencece, o alguna otra métrica para medir esto. 

Esto nos da una idea de un algoritmo simple para generar recomendaciones a
partir de sugerencias de usuarios parecidos. 

Sin embargo, tenemos un problema con la evaluación que estaba pensada para el
método. Algo que está nombrado en el paper [PlaylistMcFee p.548] es el problema
de la evaluación por predicción, ya que incluso en un amplio set de playlist,
es muy difícil que si tomo un subconjunto de mi playlist referencia, vea el
resto de las canciones en playlist vecinas (parecidas). McFee propone métodos
alternativos de evaluación.


Una nueva dirección para el trabajo práctico es buscar esta adaptación de
sistema de recomendación mediante filtro colaborativo, sosteniendo la
problemática de los datos. Luego, usar otros sistemas de recomendación
presentes en la bibliografía (estaríamos asumiendos que para varios de ellos
el código y datos está disponible) y adaptarlos para esta versión del problema
(muchos de los vistos en la bibliografía generan recomendaciones solo a partir
de una canción, en lugar de un conjunto de canciones). Finalmente, usar uno de
los mecanismos de evaluación más sofisticados y hacer comparaciones.
