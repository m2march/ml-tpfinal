import cjson
import os
import sys


def get_aotm2003():
    """Returns a dictionary playlist_id -> [song_id] from the aotm2003
    playlist dataset skipping empty playlists.

    song_id for aotm2003 is an int that does not repeat in this dataset."""
    path = os.path.join(os.path.dirname(__file__), 'aotm2003')
    f = open(path, 'r')
    lines = f.readlines()

    def get_playlist():
        for l in lines:
            pid, playlist = l.split(' ', 1)
            if not playlist.rstrip():
                continue
            art_song = playlist.rstrip().split(' ')
            assert len(art_song) % 2 == 0, ('%s: %s' % (pid, playlist))
            songs = [int(art_song[i*2 + 1]) for i in xrange(len(art_song)/2)]
            yield (int(pid.rstrip('#').lstrip('#')), songs)

    try:
        playlists = dict(get_playlist())
    except AssertionError, ae:
        print ae
    return playlists


def get_aotm2011(song_hash):
    """Returns a dictionary playlist_id -> [song_id_container] from the
    aotm2011 playlist dataset.

    song_id_container is an object that for all purposes is a song_id, but
    may have other fields or functions with further information.

    Parameters:
        song_hash: a function that recieves a song json entry and
        returns a song_id_container
    """
    path = os.path.join(os.path.dirname(__file__), 'aotm2011')
    with open(path, 'r') as file_desc:
        playlists = cjson.decode(file_desc.read())
        playlist_dict = dict([])

        for idx, playlist in enumerate(playlists):
            sys.stderr.write('\r %d/%d' % (idx+1, len(playlists)))
            playlist_id = int(playlist['mix_id'])
            songs = [song_hash(s) for s in playlist['playlist']
                     if song_hash is not None]
            playlist_dict[playlist_id] = songs

        sys.stderr.write('\n')
        return playlist_dict
