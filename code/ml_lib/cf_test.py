import cf
import unittest

train_u_i_dict = {
    1: [1, 2, 3, 4],
    2: [6, 7],
    3: [1, 2, 3],
    4: [1, 2],
    5: [1, 2, 4],
    6: [1, 2, 3, 4]
}

train_i_u_dict = {
    1: [1, 3, 4, 5, 6],
    2: [1, 3, 4, 5, 6],
    3: [1, 3, 6],
    4: [1, 5, 6],
    6: [2],
    7: [2]
}

test_u_i_dict = {
    1: [5, 6],
    2: [1, 2],
    3: [4, 5, 6],
    4: [3, 4],
    5: [5, 6, 7],
    6: [5, 6]
}


class RecommenderTest(object):

    def test_train(self):
        self.assertEquals(self.model.r_dict, train_u_i_dict)
        self.assertEquals(self.model.items_dict, train_i_u_dict)
        self.assertEquals(self.model.users_count, len(train_i_u_dict))

    def test_predictAll(self):
        self.assertTrue(type(self.predictions) == list)
        self.assertSetEqual(set([r[2] for r in self.predictions if r[0] == 1]),
                            set([r[2] for r in self.predictions if r[0] == 6]))
        self.assertTrue(5 not in [r[1] for r in self.predictions])
        self.assertTrue(all([r[2] >= 0 and r[2] <= 1
                             for r in self.predictions]))


class UxUTest(RecommenderTest, unittest.TestCase):

    @classmethod
    def setUpClass(self):
        self.model = cf.UserByUserRecommender()
        self.model.train(train_u_i_dict)
        self.predictions = self.model.predictAll(
            [(u, i) for u, ii in test_u_i_dict.items() for i in ii])

    def test_sim_matrix(self):
        self.assertEquals(len(self.model._sim_matrix), len(train_u_i_dict))
        self.assertTrue(all([len(vs) == len(train_i_u_dict) - idx - 1
                             for idx, vs in enumerate(
                                 self.model._sim_matrix.values())]))

    def test_similarities(self):
        self.assertFalse(any([self.model.sim(2, u) > 0
                              for u in [1, 3, 4, 5]]))
        self.assertEquals(self.model.sim(1, 6), 1)
        self.assertTrue([self.model.sim(1, u) == self.model.sim(6, u)
                         for u in [2, 3, 4, 5]])
        self.assertEquals(self.model.sim(3, 4), self.model.sim(4, 3))

    def test_predicted_users(self):
        predicted_users = set([r[0] for r in self.predictions])
        self.assertSetEqual(predicted_users, set([1, 3, 4, 5, 6]))

    def test_similarities_in_range(self):
        all_sims = [self.model.sim(x, y)
                    for x in [1, 2, 3, 4, 5, 6]
                    for y in [1, 2, 3, 4, 5, 6]
                    if x != y]
        self.assertTrue(all([0 <= s and s <= 1
                             for s in all_sims]))


class IxITest(RecommenderTest, unittest.TestCase):

    @classmethod
    def setUpClass(self):
        self.model = cf.ItemByItemRecommender()
        self.model.train(train_u_i_dict)
        self.predictions = self.model.predictAll(
            [(u, i) for u, ii in test_u_i_dict.items() for i in ii])

    def test_similarities(self):
        self.assertEquals(self.model.sim(1, 2), 1)
        self.assertEquals(self.model.sim(6, 7), 1)
        self.assertTrue(all([self.model.sim(a, u) == 0
                             for u in [1, 2, 3, 4]
                             for a in [6, 7]]))

    def test_similarities_in_range(self):
        all_sims = [self.model.sim(x, y)
                    for x in [1, 2, 3, 4, 6, 7]
                    for y in [1, 2, 3, 4, 6, 7]]
        self.assertTrue(all([0 <= s and s <= 1
                             for s in all_sims]))

    def test_predicted_users(self):
        predicted_users = set([r[0] for r in self.predictions])
        self.assertSetEqual(predicted_users, set([1, 2, 3, 4, 5, 6]))
