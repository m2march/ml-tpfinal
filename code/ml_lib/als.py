import cf
import logging

from pyspark.mllib.recommendation import ALS, Rating
from pyspark.rdd import RDD

logger = logging.getLogger('ALSRecommenderAdapter')


class ROALSRecommenderAdapter(cf.Recommender):

    def __init__(self, model, metadata, spark_context):
        self.model = model
        self.recommender_config = metadata['recommender_config']
        self.setup_id = metadata['setup_id']
        self.spark_context = spark_context
        self.als_type = metadata['als_type']

    def predict(self, user_id, item_id):
        return self.model.predict(user_id, item_id)

    def predictAll(self, user_item_ids):
        logger.debug('Predicting ratings for user_item_pairs (%d): %s',
                     len(user_item_ids), user_item_ids)
        if (not isinstance(user_item_ids, RDD)):
            user_item_ids = self.spark_context.parallelize(user_item_ids)
        ret = self.model.predictAll(user_item_ids).map(
            lambda x: (x.user, x.product, x.rating)).collect()
        logger.debug('Prediction results %d: %s', len(ret), ret)
        return ret

    def __str__(self):
        return '{}_r{}_i{}'.format(
            self.als_type, self.recommender_config['rank'],
            self.recommender_config['iterations'])

    def metadata(self):
        return {
            'recommender_config': self.recommender_config,
            'setup_id': self.setup_id,
            'als_type': self.als_type
        }



class ALSRecommenderAdapter(ROALSRecommenderAdapter):

    train_f = ALS.train

    def __init__(self, recommender_config, spark_context):
        self.spark_context = spark_context
        self.recommender_config = recommender_config
        self.als_type = 'als'
        assert 'rank' in recommender_config

    def train(self, users_dict):
        logger.info(
            'Starting training ALS recommender (f=%s) with users count = %s',
            self.train_f.__name__, len(users_dict)
        )

        ratings = [Rating(user_id, song_id, 1)
                   for user_id, song_ids in users_dict.items()
                   for song_id in song_ids]
        ratings = self.spark_context.parallelize(ratings)

        self.model = self.train_f(ratings, **self.recommender_config)
        logger.info(
            'Finished training ALS recommender (f=%s) with users count = %s',
            self.train_f.__name__, len(users_dict)
        )

    def desc(self):
        d = ALSRecommenderAdapter.desc(self)
        d.update(self.recommender_config)
        return d


class ALSImplicitRecommenderAdapter(ALSRecommenderAdapter):

    train_f = ALS.trainImplicit

    def __init__(self, recommender_config, spark_context):
        ALSRecommenderAdapter.__init__(self, recommender_config, spark_context)
        self.als_type = 'ials'
