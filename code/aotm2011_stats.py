# #  http://bmcfee.github.io/data/aotm2011.html

import cases
import collections
import sys
import gflags

import numpy as np
import lib.playlist_stats as ps

from lib.ascii_graph import Pyasciigraph

gflags.DEFINE_integer('sample_size', 5000,
                      'Number of playlist to use')

FLAGS = gflags.FLAGS

try:
    sys.argv = FLAGS(sys.argv)  # parse flags
except gflags.FlagsError, e:
    print '%s\\nUsage: %s ARGS\\n%s' % (e, sys.argv[0], FLAGS)
    sys.exit(1)

aotm = cases.get_aotm2011(lambda s: s[1])

# Generic Statistics
ps.print_playlist_generic_stats(aotm, 'AOTM 2011')
print ''

# UxU interaction
user_by_user_intersection = ps.get_user_by_user_interaction(
    aotm, sample_size=FLAGS.sample_size)

# Ocurrences pairs histogram
s = sorted([int(y) for y in np.nditer(user_by_user_intersection)])
histo = sorted(collections.Counter(s).items(),
               key=lambda x: x[0], reverse=True)[:-1]
graph_title = ('UxU Interaction | Total Users: (%d)' % FLAGS.sample_size)
for line in Pyasciigraph().graph(graph_title, histo):
    print line
